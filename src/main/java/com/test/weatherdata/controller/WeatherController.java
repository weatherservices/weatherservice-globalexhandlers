package com.test.weatherdata.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.weatherdata.config.Configs;
import com.test.weatherdata.model.Coord;
import com.test.weatherdata.model.WeatherData;
import com.test.weatherdata.service.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/weathers")
public class WeatherController {
    @Autowired
    private WeatherService weatherService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Configs configs;

    public WeatherController(WeatherService weatherService) {// --->as we have used @AllArgsConstructor
        this.weatherService = weatherService;
    }

    /**
    DESC - fetch all forecast weather data from repository [R]
    PATH -  /all
    params - NONE
    REQUEST - GET
    * */
    @GetMapping("/all")
    public Iterable<WeatherData> findAll() {
        return weatherService.findAll();
    }


   /* *
   DESC - save the forecast data fetched from 3rd party API to repository [C]
   PATH -  /getall
   params - NONE
   REQUEST - GET
   * */
    @GetMapping("/getall")
    public WeatherData getAllFromApi() {
        WeatherData weather = restTemplate.getForObject(configs.getUrl(), WeatherData.class);
        return weatherService.save(weather);

    }

    /* *
   DESC - delete the forecast data of given id [C]
   PATH -  /delete
   params - id
   REQUEST - DELETE
   * */
    @DeleteMapping ("/delete/{id}")  //[D]
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {
        return weatherService.delete(id);
    }

 /* *
   DESC - delete the forecast data of given id [C]
   PATH -  /delete
   params - id
   REQUEST - DELETE
   * */
    @GetMapping ("/get/{id}")  //[D]
    public WeatherData getById(@PathVariable("id") Long id) {
        return weatherService.findById(id);

    }




}
//     return weatherService.mapJSONtoJava(weather); -> ALREADY JAVA CAN MAP JSON TO OBJECTS [Serialize] USING JSON FIELDS AND OBJECT VARIABLES
