package com.test.weatherdata.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Date;

public class ErrorResponse {

    private static final long serialVersionUID = 1L;
    private String errorCode;
    private Date timerStamp;
    private String errorMessage;
    public String getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Date getTimerStamp() {
        return timerStamp;
    }

    public void setTimerStamp(Date timerStamp) {
        this.timerStamp = timerStamp;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public ErrorResponse(String errorCode, String errorMessage, Date date) {
        super();
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.timerStamp = date;
    }

    public ErrorResponse() {

    }

    }
