package com.test.weatherdata.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.weatherdata.exceptions.ElementAreadyExistsException;
import com.test.weatherdata.exceptions.WeatherDataException;
import com.test.weatherdata.model.*;
import com.test.weatherdata.repository.WeatherRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;


@Service
@Configuration
public class WeatherService {

    private final WeatherRepository weatherRepository;
    public WeatherService(WeatherRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    @Bean
    public WeatherService getweatherService(){

        return new WeatherService(weatherRepository);
    }

    public Iterable<WeatherData> findAll() { //[R]
            return weatherRepository.findAll();
    }

    public WeatherData save(WeatherData weatherData) //[C]
    {
        if (weatherRepository.existsById(weatherData.getCnt())){
            throw new ElementAreadyExistsException();
        }
       return weatherRepository.save(weatherData);
    }

     public ResponseEntity<String> delete(Long id) //[D]
    {
        weatherRepository.deleteById(id);
       return new ResponseEntity<>("Deleted successfully", HttpStatus.ACCEPTED);
    }

    public WeatherData findById(Long id) //[R]
    {
           return weatherRepository.findById(id).get();
    }



    //NO - NEED OF THIS ALSO
    public WeatherData mapJSONtoJava(WeatherData weatherData){
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonString = mapper.writeValueAsString(weatherData);

            WeatherData weatherDataObj = mapper.readValue(jsonString, WeatherData.class);
            System.out.println("----------------Saving- data -------------------------------------------------------------------");
            System.out.println(weatherDataObj.toString());
            WeatherData resultData =  weatherRepository.save(weatherDataObj);
            System.out.println("----------------Saved- -------------------------------------------------------------------");

            return resultData;

        }catch (Exception e){
            e.printStackTrace();
        }

        return weatherData;
    }



}



